"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var wait = function wait() {
  var delay = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  return new Promise(function (resolve) {
    return setTimeout(resolve, delay);
  });
};

var setVisible = function setVisible(elementOrSelector, visible) {
  return (typeof elementOrSelector === 'string' ? document.querySelector(elementOrSelector) : elementOrSelector).style.display = visible ? 'block' : 'none';
};

setVisible('.page', false);
setVisible('#loading', true);
document.addEventListener('DOMContentLoaded', function () {
  return wait(1).then(function () {
    setVisible('.page', true);
    setVisible('#loading', false);
    jQuery(function ($) {
      var _Swiper;

      $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();
        $('html, body').animate({
          scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
      });
      $('.phone').inputmask({
        mask: '+7(999)-999-99-99',
        showMaskOnHover: false
      });
      $('.l-docs-card').on('click', function (e) {
        $('.l-docs-modal__title').html($(this).find('.l-docs-card__title').html());
        $('.l-docs-modal__what').html($(this).find('.l-docs-card__text').html());
        $('.l-docs-modal__text').html($(this).data('text'));
        $('.l-docs-modal__image').css('background-image', $(this).find('.l-docs-card__image').css('background-image'));
        $('.l-docs-modal__image').attr('href', $(this).find('.l-docs-card__image').css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, ''));
        $('.l-docs-modal').toggle();
      });
      $('.l-docs-modal__centered').on('click', function (e) {
        if (e.target.className === 'l-docs-modal__centered') {
          $('.l-docs-modal').hide();
        }
      });
      $('.l-docs-modal__close').on('click', function (e) {
        $('.l-docs-modal').hide();
      });
      $('.open-modal').on('click', function (e) {
        $('.l-modal').fadeIn();
      });
      $('.l-modal__centered').on('click', function (e) {
        if (e.target.className === 'l-modal__centered') {
          $('.l-modal').fadeOut();
        }
      });
      $('.l-modal__close').on('click', function (e) {
        $('.l-modal').fadeOut();
      });
      $('.open-thx').on('click', function (e) {
        $('.l-thx').fadeIn();
      });
      $('.l-thx__centered').on('click', function (e) {
        if (e.target.className === 'l-thx__centered') {
          $('.l-thx').fadeOut();
        }
      });
      $('.l-thx__close').on('click', function (e) {
        $('.l-thx').fadeOut();
      });
      $('.l-header__toggle').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('l-header__toggle_active');
        $('.l-menu').slideToggle('fast');
      });
      $('.l-menu__nav li a').on('mouseover', function (e) {
        $('.l-menu__image').css('background-image', 'url(' + $(this).data('url') + ')');
      });
      $('.l-service-info-collapse__title').on('click', function (e) {
        e.preventDefault();
        $(this).parent().toggleClass('l-service-info-collapse_active');
        $(this).next().slideToggle('fast');
      });

      if (window.matchMedia('(max-width: 1200px)').matches) {
        AOS.init();
        $('.l-header__nav').detach().prependTo('.l-menu__nav');
        $('.l-header__phone').detach().insertAfter('.l-menu__nav');
        $('.l-home-about__button').detach().insertAfter('.l-home-about__cards');
        $('.l-footer-partners').detach().insertAfter('.l-footer_help_partners');
        $('.l-projects-aside__button_more').detach().insertAfter('.l-projects__cards');
        $('.l-object-similar .l-home-docs__row').detach().insertAfter('.l-object-similar__cards');
        $('.l-service-include__content').on('click', function (e) {
          e.preventDefault();
          $(this).toggleClass('l-service-include__content_active');
        });
        $('.l-projects-aside__title').on('click', function (e) {
          e.preventDefault();
          $(this).toggleClass('l-projects-aside__title_active');
          $(this).next().slideToggle('fast');
        });
        /* $(".l-object-items .l-home-docs__row")
          .detach()
          .insertAfter(".l-object-items__title"); */

        new Swiper('.l-about-companies__cards', {
          slidesPerView: 1,
          spaceBetween: 30
        });
        new Swiper('.l-about-team__cards', {
          slidesPerView: 1,
          spaceBetween: 30
        });
        new Swiper('.l-about-docs__cards', {
          slidesPerView: 1,
          spaceBetween: 30,
          pagination: {
            el: '.l-about-docs .swiper-pagination',
            type: 'fraction',
            formatFractionCurrent: function formatFractionCurrent(number) {
              return ('0' + number).slice(-2);
            },
            formatFractionTotal: function formatFractionTotal(number) {
              return ('0' + number).slice(-2);
            }
          },
          navigation: {
            nextEl: '.l-about-docs .swiper-button-next',
            prevEl: '.l-about-docs .swiper-button-prev'
          }
        });
        $('.l-about-companies__cards .swiper-slide').first().detach().insertAfter('.l-about-companies__text').css({
          marginBottom: '40px'
        });
      } else {
        // The viewport is at least 1200 pixels wide
        new Swiper('.mainSlider', {
          direction: 'vertical',
          mousewheel: true,
          pagination: {
            el: '#main-pagination',
            clickable: true
          },
          on: {
            init: function init() {
              $('.swiper-slide-active [data-aos]').each(function () {
                $('.l-header').addClass('aos-animate');
                $(this).addClass('aos-animate');
              });
            },
            slideChangeTransitionEnd: function slideChangeTransitionEnd() {
              $('.swiper-slide-active [data-aos]').each(function () {
                $(this).addClass('aos-animate');
              });
            }
          }
        });
        $('.l-object-gallery__cards').css({
          maxHeight: $('.l-object-gallery-card').outerHeight() * 3
        });
      }

      $('.l-about-geo__map svg circle').each(function () {
        $(this).addClass('circle_green');
      });
      $('.circle_green').each(function () {
        $(this).css({
          fill: '#404040'
        });
      });
      $(window).scroll(function () {
        var top_of_element = $('.l-about-geo__title').offset().top;
        var bottom_of_element = $('.l-about-geo__title').offset().top + $('.l-about-geo__title').outerHeight();
        var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
        var top_of_screen = $(window).scrollTop();

        if (bottom_of_screen > top_of_element && top_of_screen < bottom_of_element) {
          // the element is visible, do something
          $('.circle_green').each(function (index) {
            $(this).delay(50 * index).queue(function (index) {
              $(this).css({
                fill: '#95c132'
              });
            });
          });
        } else {// the element is not visible, do something else
        }
      });
      new Swiper('.l-home-docs__cards', {
        loop: true,
        spaceBetween: 30,
        pagination: {
          el: '.l-home-docs .swiper-pagination',
          type: 'fraction',
          formatFractionCurrent: function formatFractionCurrent(number) {
            return ('0' + number).slice(-2);
          },
          formatFractionTotal: function formatFractionTotal(number) {
            return ('0' + number).slice(-2);
          }
        },
        navigation: {
          nextEl: '.l-home-docs .swiper-button-next',
          prevEl: '.l-home-docs .swiper-button-prev'
        }
      });
      new Swiper('.l-home-docs__clients', {
        autoplay: {
          delay: 2500,
          disableOnInteraction: false
        },
        loop: true,
        spaceBetween: 30,
        slidesPerView: 1,
        breakpoints: {
          1200: {
            slidesPerView: 6
          }
        }
      });
      new Swiper('.l-footer-partners__cards', {
        autoplay: {
          delay: 2500,
          disableOnInteraction: false
        },
        loop: true,
        spaceBetween: 30,
        slidesPerView: 1,
        breakpoints: {
          1200: {
            slidesPerView: 5
          }
        }
      });
      new Swiper('.l-object-items__cards', {
        loop: true,
        spaceBetween: 20,
        pagination: {
          el: '.l-object-items .swiper-pagination',
          type: 'fraction',
          formatFractionCurrent: function formatFractionCurrent(number) {
            return ('0' + number).slice(-2);
          },
          formatFractionTotal: function formatFractionTotal(number) {
            return ('0' + number).slice(-2);
          }
        },
        navigation: {
          nextEl: '.l-object-items .swiper-button-next',
          prevEl: '.l-object-items .swiper-button-prev'
        },
        breakpoints: {
          760: {
            slidesPerView: 2,
            spaceBetween: 30
          },
          1200: {
            slidesPerView: 4,
            spaceBetween: 30
          }
        }
      });
      new Swiper('.l-object-similar__cards', (_Swiper = {
        loop: true,
        spaceBetween: 0
      }, _defineProperty(_Swiper, "spaceBetween", 30), _defineProperty(_Swiper, "pagination", {
        el: '.l-object-similar .swiper-pagination',
        type: 'fraction',
        formatFractionCurrent: function formatFractionCurrent(number) {
          return ('0' + number).slice(-2);
        },
        formatFractionTotal: function formatFractionTotal(number) {
          return ('0' + number).slice(-2);
        }
      }), _defineProperty(_Swiper, "navigation", {
        nextEl: '.l-object-similar .swiper-button-next',
        prevEl: '.l-object-similar .swiper-button-prev'
      }), _defineProperty(_Swiper, "breakpoints", {
        760: {
          slidesPerView: 2,
          spaceBetween: 0
        },
        1200: {
          slidesPerView: 3,
          spaceBetween: 0
        }
      }), _Swiper));
    });
  });
});
//# sourceMappingURL=main.js.map